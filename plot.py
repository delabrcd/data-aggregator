import pandas as pd
import numpy as np
import plotly.graph_objects as go
from datetime import *
firsdate = datetime(2021, 1, 1)
lastdate = datetime(2022, 4, 1)
# import data from csv
df = pd.read_csv("GR-1001.RMH.csv", parse_dates=["Date"])
# rename value to temp for clarity
df = df.rename({"Value": "Temperature"}, axis='columns')

df2 = pd.read_csv("GR-1001-FCU-001.FAN.csv", parse_dates=["Date"])
# rename value to fan for clarity
df2 = df2.rename({"Value": "Fan"}, axis='columns')

# make sure the temp values are being read in as numbers, if there isnt a number there "coerce" it (ignore strings)
df["Temperature"] = df["Temperature"].apply(pd.to_numeric, errors='coerce')

# set index to date
df = df.set_index('Date')
# sort by date
df = df.sort_index()
# resample, hourly, taking the mean by date
df = df.resample('H').mean()
# truncate by date
df = df.truncate(firsdate, lastdate)
# reset index (otherwise things get wonky during graphing)
df = df.reset_index()

# set index to date
df2 = df2.set_index("Date")
# sort by date
df2 = df2.sort_index()
# truncate by date
df2 = df2.truncate(firsdate, lastdate)
# reset index (otherwise things get wonky during graphing)
df2 = df2.reset_index()

# add an energy column consisting of the gradient of temp
df["Energy"] = np.gradient(df["Temperature"])

# convert th efan column to 0 and 100 values
df2["Fan"] = df2["Fan"].map({"ON": 100, "OFF": 0})

# define a new figure to plot onto
fig = go.Figure()
# for more info on how to use the layout, see https://plotly.com/python/reference/layout/
# each parameter should be a dictionary (curly braces with a corresponding word and value (i.e. title : "Test"))
# most of these parameters should be nested dictionaries (dict inside a dict, i.e. font)
title_formatting = {"text": "Test",
                    "font": {"color": "blue",
                             "size": 24,
                             "family": "Courier New"
                             }
                    }

legend_formatting = {"bgcolor" : "purple",
                     "bordercolor" : "orange",
                     "borderwidth" : 5,
                     "title" : {"text" : "yep"}
                    }
# update the layout of the figure
fig.update_layout(title=title_formatting, legend=legend_formatting)
# add a trace for the fan - these get layed on top of each other so do it first so its at the bottom
# the line parameter makes it graph a proper step function, the fill parameter sets the area fill
fig = fig.add_trace(go.Scatter(x=df2["Date"], y=df2["Fan"], line={
                    "shape": "hv"}, fill="tozeroy", name="Fan State"))
fig = fig.add_trace(go.Scatter(
    x=df["Date"], y=df["Temperature"], mode="lines", name="Temperature"))
fig = fig.add_trace(go.Scatter(
    x=df["Date"], y=df["Energy"], mode="lines", name="Energy"))
fig.show()
