from lib2to3.pytree import Base
import tkinter as tk
from tkinter import BooleanVar, ttk, StringVar, IntVar, filedialog, simpledialog, messagebox
import plotly.graph_objects as go
from datetime import *
import pandas as pd
from os import path
from dataclasses import dataclass
from threading import Thread, Lock
import numpy as np

firsdate = datetime(2021, 1, 1)
lastdate = datetime(2022, 4, 1)

datatypes = ["Temperature", "Temperature Gradient", "Fan"]

class BaseDlg(simpledialog.Dialog):
    def __init__(self, parent, title="", **kw):
        self.exitcode = IntVar(value=0)
        super().__init__(parent, title)

    def apply(self):
        self.exitcode.set(1)


class GetFileInfoDlg(BaseDlg):
    def body(self, master):
        self.datatype = StringVar()
        self.axisname = StringVar()
        self.averagehourly = BooleanVar()
        for type in datatypes:
            rb = ttk.Radiobutton(
                self, text=type, variable=self.datatype, value=type)
            rb.pack()
        self.cb = ttk.Checkbutton(
            self, text="Average Hourly", variable=self.averagehourly)
        self.cb.pack()

        ttk.Label(self, text="Data Label:").pack()

        self.e1 = ttk.Entry(self, textvariable=self.axisname)
        self.e1.pack()
        return self.e1  # initial focus


class GraphSettingsDlg(BaseDlg):
    def __init__(self, parent, title, curtitle="", curx="", cury="", curlegend="", **kw):
        self.exitcode = IntVar(value=0)
        self.graphtitle = StringVar(value=curtitle)
        self.ylabel = StringVar(value=cury)
        self.xlabel = StringVar(value=curx)
        self.legendtitle = StringVar(value=curlegend)
        super().__init__(parent, title, kw=kw)

    def body(self, master):

        self.title_label = ttk.Label(self, text='Title:',
                                     font=('calibre', 10, 'bold'))
        self.title = ttk.Entry(self, textvariable=self.graphtitle,
                               font=('calibre', 10, 'normal'))

        self.x_axis_label = ttk.Label(
            self, text='X-Axis Title:', font=('calibre', 10, 'bold'))
        self.x_axis_entry = ttk.Entry(self, textvariable=self.xlabel,
                                      font=('calibre', 10, 'normal'))

        self.y_axis_label = ttk.Label(
            self, text='Y-Axis Title:', font=('calibre', 10, 'bold'))
        self.y_axis_entry = ttk.Entry(self, textvariable=self.ylabel,
                                      font=('calibre', 10, 'normal'))

        self.legend_label = ttk.Label(
            self, text='Legend Title:', font=('calibre', 10, 'bold'))
        self.legend_entry = ttk.Entry(self, textvariable=self.legendtitle,
                                      font=('calibre', 10, 'normal'))

        self.title_label.pack()
        self.title.pack()
        self.x_axis_label.pack()
        self.x_axis_entry.pack()
        self.y_axis_label.pack()
        self.y_axis_entry.pack()
        self.legend_label.pack()
        self.legend_entry.pack()
        return self.title  # initial focus


class MainApplication(tk.Frame):

    def disable_children(self):
        for w in self.winfo_children():
            if isinstance(w, ttk.Button):
                w.configure(state="disabled")

    def enable_children(self):
        for w in self.winfo_children():
            if isinstance(w, ttk.Button):
                w.configure(state="enabled")

    def create_graph_detached(self):
        fig = go.Figure()
        self.treeviewmtx.acquire()
        root.config(cursor="wait")
        self.disable_children()
        for item in self.treeview.get_children():
            tracename = datatype = self.treeview.item(item)['values'][0]
            filename = self.treeview.item(item)['values'][1]
            datatype = self.treeview.item(item)['values'][2]
            averagehourly = self.treeview.item(item)['values'][3] == "True"
            df = pd.read_csv(filename, parse_dates=["Date"])
            df = df.set_index('Date')
            df = df.sort_index()
            df = df.truncate(firsdate, lastdate)
            if averagehourly:
                df = df.resample('H').mean()

            df = df.reset_index()
            if datatype == "Fan":
                df["Value"] = df["Value"].map({"ON": 100, "OFF": 0})
                fig = fig.add_trace(go.Scatter(
                    x=df["Date"], y=df["Value"], mode="lines", line={
                        "shape": "hv"}, fill="tozeroy", name=tracename))
            elif datatype == "Temperature Gradient":
                df["Value"] = np.gradient(df["Value"])
                fig = fig.add_trace(go.Scatter(
                    x=df["Date"], y=df["Value"], mode="lines", name=tracename))
            else:
                fig = fig.add_trace(go.Scatter(
                    x=df["Date"], y=df["Value"], mode="lines", name=tracename))

        title_formatting = {"text": self.titletext,
                            "font": {"color": "black",
                                     "size": 24,
                                     }
                            }

        legend_formatting = {
            "title": {"text": self.legendtext}
        }

        yaxis_formatting = {"title": {"text": self.ylabeltext}}
        fig.update_yaxes(yaxis_formatting)

        xaxis_formatting = {"title": {"text": self.xlabeltext}}
        fig.update_xaxes(xaxis_formatting)
        # update the layout of the figure
        fig.update_layout(title=title_formatting,
                          legend=legend_formatting)
        fig.show()
        root.config(cursor="")
        self.treeviewmtx.release()
        self.enable_children()

    def add_trace_from_file(self):
        for file in filedialog.askopenfilenames(filetypes=[('.csv files', '.csv')]):
            basefile = path.basename(file)
            test = GetFileInfoDlg(self, title=basefile)
            if not test.exitcode.get():
                return
            self.treeviewmtx.acquire()
            self.treeview.insert('', 'end', values=(
                test.axisname.get(), file, test.datatype.get(), test.averagehourly.get()))
            self.treeviewmtx.release()

    def graph(self):
        t = Thread(target=self.create_graph_detached)
        t.start()

    def get_new_settings(self):
        settings = GraphSettingsDlg(
            self, "Set Graph Settings", curtitle=self.titletext, curx=self.xlabeltext, cury=self.ylabeltext, curlegend=self.legendtext)
        if not settings.exitcode.get():
            return
        self.titletext = settings.graphtitle.get()
        self.ylabeltext = settings.ylabel.get()
        self.xlabeltext = settings.xlabel.get()
        self.legendtext = settings.legendtitle.get()

    def remove_trace(self):
        self.treeviewmtx.acquire()
        selected_items = self.treeview.selection()
        for selected_item in selected_items:
            self.treeview.delete(selected_item)
        self.treeviewmtx.release()

    def move_up(self):
        self.treeviewmtx.acquire()
        leaves = self.treeview.selection()
        for i in leaves:
            self.treeview.move(i, self.treeview.parent(i),
                               self.treeview.index(i)-1)
        self.treeviewmtx.release()

    def move_down(self):
        self.treeviewmtx.acquire()
        leaves = self.treeview.selection()
        for i in reversed(leaves):
            self.treeview.move(i, self.treeview.parent(i),
                               self.treeview.index(i)+1)
        self.treeviewmtx.release()

    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.treeviewmtx = Lock()
        self.titletext = ""
        self.ylabeltext = ""
        self.xlabeltext = ""
        self.legendtext = ""

        self.parent = parent
        self.treeview = ttk.Treeview(
            self, columns=(1, 2, 3, 4), show='headings')
        self.treeview.heading(1, text="Trace Name")
        self.treeview.heading(2, text="Filename")
        self.treeview.heading(3, text="Data Type")
        self.treeview.heading(4, text="Average Hourly")

        self.addbutton = ttk.Button(
            self, text="+", command=self.add_trace_from_file)
        self.removebutton = ttk.Button(
            self, text="-", command=self.remove_trace)
        self.moveupbutton = ttk.Button(
            self, text="↑", command=self.move_up)
        self.movedownbutton = ttk.Button(
            self, text="↓", command=self.move_down)
        self.graphsettingsbutton = ttk.Button(
            self, text="Graph Settings", command=self.get_new_settings)
        self.graphbutton = ttk.Button(
            self, text="Graph!", command=self.graph)

        self.treeview.pack(side="top", fill='x', padx=10, pady=10)
        self.graphbutton.pack(side="right", padx=10, pady=10)
        self.addbutton.pack(side="right", padx=10, pady=10)
        self.removebutton.pack(side="right", padx=10, pady=10)
        self.moveupbutton.pack(side="right", padx=10, pady=10)
        self.movedownbutton.pack(side="right", padx=10, pady=10)
        self.graphsettingsbutton.pack(side="right", padx=10, pady=10)


if __name__ == "__main__":
    root = tk.Tk()
    MainApplication(root).pack(side="top", fill="both", expand=True)
    root.mainloop()
