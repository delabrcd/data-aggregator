from dataclasses import dataclass
from datetime import *
from os import name
from pprint import pprint
import csv

import tkinter as tk
from tkinter import filedialog
import glob
from typing import final


@dataclass
class Node:
    name: str
    location: str
    data: list


@dataclass
class GZoneData:
    time: datetime
    status: str


@dataclass
class GZoneHeatData(GZoneData):
    temp: float


@dataclass
class GZoneFanData(GZoneData):
    state: bool


def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False


def serializecsvdata(filename, dict):
    f = open(filename, "r")
    gzone = Node("", "", [])
    floatdata = False
    while True:
        line = f.readline()
        if not line:
            break
        list = line.strip().split(',')
        if not list[0]:
            continue
        elif(list[0] == ("Point System Name:")):
            if gzone.name and gzone.name in dict:
                dict[gzone.name].data = dict[gzone.name].data + gzone.data
            elif gzone.name:
                dict[gzone.name] = gzone
            gzone = Node("", "", [])
            gzone.name = list[1]
            floatdata = False
        elif(list[0] == "Trend Location (Local):"):
            gzone.location = list[1]
        elif(list[0] == "Trend Every:"):
            continue
        elif(list[0] == "Date Range:"):
            continue
        elif(list[0] == "Report Timings:"):
            continue
        elif(list[0] == "No data in range specified."):
            continue
        elif(list[0] == "Data Loss"):
            continue
        elif(list[0].__contains__("****************")):
            continue
        else:
            date_time_str = list[0] + " " + list[1]
            if(isfloat(list[2])):
                floatdata = True
                heatdata = [datetime.strptime(
                    date_time_str, '%m/%d/%Y %H:%M:%S'), list[3], float(list[2])]
                gzone.data.append(heatdata)
            elif list:
                if floatdata:
                    continue
                fandata = [datetime.strptime(
                    date_time_str, '%m/%d/%Y %H:%M:%S'), list[3], list[2]]
                gzone.data.append(fandata)


def main():
    root = tk.Tk()
    root.withdraw()
    dir_path = filedialog.askdirectory()
    file_paths = glob.glob(dir_path + "/*.csv")
    finaldict = {}
    count = 1
    for file in file_paths:
        print("Starting on file " + str(count) + " / " + str(len(file_paths)))
        serializecsvdata(file, finaldict)
        count += 1

    for item in finaldict:
        with open(item + ".csv", 'w', newline='', encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerow(["Date", "State", "Value"])
            for index, dataline in finaldict[item].data:
                writer.writerow(dataline)


if __name__ == "__main__":
    main()
